﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _201014Win_RestoranUygulaması
{
    public partial class MasaDetayForm : Form
    {
        public MasaDetayForm()
        {
            InitializeComponent();
        }
        public Masa Masa { get; set; }
        private void MasaDetayForm_Load(object sender, EventArgs e)
        {
            lblMasaNo.Text = Masa.Kodu;
            lblTutar.Text = Masa.Tutar.ToString("C2");//decimal veri tipinde olan değişkenlerinde tostringine gidip c2 gibi değerler verdiğimizde yazım stilini değiştiyoruz
            foreach (Urun u in Masa.Urunler)
            {
                listView1.Items.Add(u.ListOlustur());//list oluştur meodu listview ıtem a dönüştürüp veriyordu listview item haline dönüştütülmüş olarak veriyor
            }

           // cmbUrun.DisplayMember = "Adi";//gösterilecek olan değeri verir display özelliği.cmburun içerisinde ürün nesnesini tutar fakat adı özelliğini gösterir
           // cmbUrun.ValueMember = "Fiyat";
            //combobox gibi ürünlerin datasource isminde özelliği var
            cmbUrun.DataSource = Urun.Liste;//ürünleri direk datasource ile combobox a bağlamış oluyoruz
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            Urun u = (Urun)cmbUrun.SelectedItem;
            Masa.Urunler.Add(u);
            listView1.Items.Add(u.ListOlustur());
        }
    }
}
