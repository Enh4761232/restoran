﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _201014Win_RestoranUygulaması
{
    //Access Modifiers
    /*
     * private:Sadece sınıf içinde(varsayılan)
     * protected:Miras alınan sınıflarda kullanılabilir.
     * internal:Proje içidekiler kullanabilir(namespace)
     * protected internal:Hem proje içindeki her yerde.Hem miras alana aktarılır anlamına gelir.
     * public:Her yerden erişilebilir demektir
    */

    //Constructor Method
    /*
     * Bir sınıftan instance alınırken (yani new komutu kullanılarak yeni bir değişken üretirken.)Çalışmasını istediğimiz komutları constructorda belirtebiliyoruz.Yani sınıf doğarken komut çalıştırmaktır.
     * Constuructor;
     * Sınıf ile aynı isimde olmalıdır
     * Geriye dönüş tipi olmayan bir metotdur.
     * overload yapılabilir.(Bir metodun aynı isimle birden fazla kez tanımlanabilmesidir.)
     * 
     * 
     * 
     * POLYMORPHIZM
     * 
     * Bir metodun miras alındığı sınıfta ezilebilmesi için değiştirebilmesi için o metodun virtual olarak işaretlenmesi gerekiyor
     * 
     * 
     */
    //stringlerin + operatörü ile birleştirilmeside bir overloaddır.Stringlerin == operatorüyle karşılaştırılmasıda bir overload ile sağlanmıştır


    public class Urun
    {
        public Urun()
        {
            //new Urun(); urun metodunu overload ettik iki çeşit kullanımı oldu
            //overloadın tek şartı metodun izasının değişmesi.yani parametreler metodun imzasıdır.
            //Liste.Add(this);//BU SINIFI LİSTEYE EKLE DENİİYOR CONSTURUCTOR ANINDA new Urun denildiğinde listeye o ürünü atıyor
            //this kavramı ramde üretilen instanceyi kastediyor
            
        }
        public Urun(string adi,decimal fiyat)
        {
            Adi = adi;
            Fiyat = fiyat;
            Liste.Add(this);//this üretilmiş olan instanceyi kasteder.Şuan çalıştığım sınıftaki instanceyi bana getir demektir.

        }
        //public Urun(decimal fiyat,string adi)
        //{

        //}
        //metot imzaları
        //Urun(string,decimal)
        //Urun(decimal,string)
        

        public string Adi;
        public decimal Fiyat;

        public static List<Urun> Liste = new List<Urun>();

        public virtual ListViewItem ListOlustur()
        {
            ListViewItem li = new ListViewItem();
            li.Text = Adi;
            li.SubItems.Add(Fiyat.ToString("C2"));
            return li;

        }
        //field(değişken)
        //Method
        //event
        public override string ToString()
        {
            return this.Adi;
        }

    }
    public class EsantiyonUrun:Urun
    {
        public EsantiyonUrun()
        {
           
        }
        public override ListViewItem ListOlustur()
        {

            // return base.ListOlustur(); otomatik oluştuğunda base sınıfaki metot gelir
            ListViewItem li = new ListViewItem();
            li.Text = this.Adi;
            return li;

            
        }
    }

}
