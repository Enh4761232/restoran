﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _201014Win_RestoranUygulaması
{
    public partial class SiparisForm : Form
    {
        public SiparisForm()
        {
            InitializeComponent();
        }

        private void SiparisForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MasaListeForm m = new MasaListeForm();
            m.Show();
        }

        private void SiparisForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Controls.Clear();
            //form kapandığında kapanan formun içindeki kontrolleri siliyor.
            //amaç ramden silmek

        }
    }
}
