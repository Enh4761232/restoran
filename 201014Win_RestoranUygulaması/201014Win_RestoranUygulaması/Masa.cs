﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _201014Win_RestoranUygulaması
{
    //Classlarda yapı oluşturulduğundan platform belli değildir form elemanlarına ulaşmaya çalışmak class yapısına terstir.Doğrudan arayüz erişimi yapılmaz.
    /*Encapsulation
     * 
     * 
     * Property:Değişkenlere değer alıp atmak için komut çalıştıran ancak değişken gibi davranan bir yapıdır.
     
     */
    //burası bir sınıf arayüz ile bağlantısız düşünerek yazılmalı

    class Masa
    {
        //public void _Kodu
        //{
        //   this.Text=this.Kodu;
        //   this.Width=70;
        //   this.Height=70;

        //}
            
        private string _Kodu;
        public string Kodu
        {
            get { return _Kodu; }
            set { _Kodu = this.Text = value; }
        }
       
        private string _MusteriAdi;//field Değeri tutar
        public string MusteriAdi//property değere erişir
        {
            get { return _MusteriAdi; }
         
            set //value setin içerisinde olan local değişken.value dışarıdan gelen değeri gönderir.
            {
                if(value.Length<2)
                {
                    //throw hata fırlat demektir
                    throw new Exception("İsim 2 harften küçük olamaz");//kod buraya düşer ise yeni hata oluşturur hata fırlatılmıştır.
                   
                }
                _MusteriAdi = value;
            }
        }
        private string musteriTelefon;
        //fieldleri seçerek ctrl+R+E yapıldığında property oluşturur
        public string MusteriTelefon
        { 
            get => musteriTelefon; 
            set //setin önemli özelliklerinden birside event tetiklemektir.
            {
                if (value.Length < 7 && value.Length > 0)
                    throw new Exception("Telefon numarasını standartlara uygun giriniz");
                musteriTelefon = value;
            }
                
 }


        public bool Rezerve = false;
        public bool Dolu = false;
        List<Urun> _Urunler = new List<Urun>();
        //hesabı çıkarabilmek için ürünler gerekli birden fazla olduğu için List tipinde tanımlandı
        //List<Urun> Urunler; olarak tanımlandığında stacte Uurnler=null kalıyor
        //Urunler.Add(m);//ürünler referansına gitmeye çalışacak  state referansı null olduğundan hata verecek
        //buna çözüm olarak direk classın içerisinde List<Urun> Urunler = new List<Urun>(); instance oluşturuyoruz.ve heapte referans tipli olarak oluşturuluyor.
        public List<Urun> Urunler
        {
            get => _Urunler;
            set => _Urunler = value;
        }
        public decimal Tutar
        {
            get
            {
                decimal s = 0;
                foreach (Urun u in Urunler)
                {
                    s += u.Fiyat;
                }
                return s;
            }

        }


    }
}
