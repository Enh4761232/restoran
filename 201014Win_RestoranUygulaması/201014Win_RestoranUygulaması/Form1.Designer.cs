﻿namespace _201014Win_RestoranUygulaması
{
    partial class Anaform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.siparişToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ürünToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salonToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.siparişToolStripMenuItem1,
            this.ürünToolStripMenuItem2,
            this.salonToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1415, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // siparişToolStripMenuItem1
            // 
            this.siparişToolStripMenuItem1.Name = "siparişToolStripMenuItem1";
            this.siparişToolStripMenuItem1.Size = new System.Drawing.Size(67, 24);
            this.siparişToolStripMenuItem1.Text = "Sipariş";
            // 
            // ürünToolStripMenuItem2
            // 
            this.ürünToolStripMenuItem2.Name = "ürünToolStripMenuItem2";
            this.ürünToolStripMenuItem2.Size = new System.Drawing.Size(54, 24);
            this.ürünToolStripMenuItem2.Text = "Ürün";
            this.ürünToolStripMenuItem2.Click += new System.EventHandler(this.ürünToolStripMenuItem2_Click);
            // 
            // salonToolStripMenuItem1
            // 
            this.salonToolStripMenuItem1.Name = "salonToolStripMenuItem1";
            this.salonToolStripMenuItem1.Size = new System.Drawing.Size(60, 24);
            this.salonToolStripMenuItem1.Text = "Salon";
            this.salonToolStripMenuItem1.Click += new System.EventHandler(this.salonToolStripMenuItem1_Click);
            // 
            // Anaform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1415, 746);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Anaform";
            this.Text = "Anaform";
            this.Load += new System.EventHandler(this.Anaform_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem siparişToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ürünToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salonToolStripMenuItem1;
    }
}

