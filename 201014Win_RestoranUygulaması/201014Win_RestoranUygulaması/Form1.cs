﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _201014Win_RestoranUygulaması
{
    public partial class Anaform : Form
    {
        //5 temel tip
        //Class
        //interface
        //struct
        //enum
        //delegate

        //5 temel kural
        //Encapsulation
        //Inheritance
        //Polymorphizm
        //Constructor
        //AccessModifiers

        public Anaform()
        {
            InitializeComponent();
        }

        private void ürünToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UrunForm u = new UrunForm();
            u.Show();
        }

        private void Anaform_Load(object sender, EventArgs e)
        {
            //IsMdiContainer özelliğini true yaptık
        }

        private void salonToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SalonForm s = new SalonForm();
            s.Show();
        }
    }
}
