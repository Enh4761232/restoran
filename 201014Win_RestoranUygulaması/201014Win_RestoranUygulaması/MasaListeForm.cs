﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _201014Win_RestoranUygulaması
{
    public partial class MasaListeForm : Form
    {
        

        public MasaListeForm()
        {
            InitializeComponent();
        }
        public Salon Salon { get; set; }
        private void MasaListeForm_Load(object sender, EventArgs e)
        {
            lblSalonAdi.Text = Salon.Adi;
            foreach (Masa m in Salon.Masalar)
            {
                m.Click += masa_Click;
                flowLayoutPanel1.Controls.Add(m);
            }
            //run time olmayan bir elemana çift tıklayıp click olayına giremem.event bağlamak için masa click metodu bağlıyoruz
        }
        void masa_Click(object sender,EventArgs e)
        {
            //bu metot 15 masa varsa 15 ine tıklayınca da çalışıyor
            //hangisine tıkladığımızı ayırt etmem gerekiyor
            //bu metodun çalışmasına ne sebep oluyor ise o sender ın içerisine gönderiliyor.
            //hangi kontrol olursa olsun senderın içerisine gönderilir.hangi eleman bu metodu çalıştırdı ise sender ın içerisinde bulunuyor.
            //herşey masa classının içerisinde döndüğünden sender objesi masa clası olmalıdır.
            MasaDetayForm mdf = new MasaDetayForm();
            mdf.Masa = (Masa)sender;
            mdf.MdiParent = this.MdiParent;//benim babam seninde baban
            mdf.Show();

        }

    }
}
